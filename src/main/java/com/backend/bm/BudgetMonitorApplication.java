package com.backend.bm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BudgetMonitorApplication {
	public static void main(String[] args) {
		SpringApplication.run(BudgetMonitorApplication.class, args);
	}
}